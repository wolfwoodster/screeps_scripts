var roles = require('roles');
var utils = require('utils');

var manager = {}

/*
Creep naming system:
role_serial_#WORK_#CARRY_#MOVE_#CARRY_#ATTACK_#RANGED_ATTACK_#HEAL_#CLAIM
*/

manager.roles = {
    harvester: roles.harvester.run,
    upgrader: roles.upgrader.run,
    builder: roles.builder
}

manager.manageCreeps = function(){
    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        manager.roles[creep.memory.role](creep);
    }
};

manager.maintainPopulation = function(spawner){
    //console.log("Attempting to spawn from " + spawner);
    for(var name in Memory.creeps) {
        if(!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log('Clearing non-existing creep memory:', name);
        }
    }
    var harvesters = _.filter(Game.creeps, (creep) => creep.memory.role == 'harvester');
     if(harvesters.length < 2) {
        console.log('Harvesters: ' + harvesters.length);
        if(Game.spawns[spawner].canCreateCreep([WORK,CARRY,MOVE], undefined ) === 0){
            var newName = Game.spawns[spawner].createCreep([WORK,CARRY,MOVE], undefined, {role: 'harvester'});
            console.log('Spawning new harvester: ' + newName);
        }
    }
};


module.exports = manager;