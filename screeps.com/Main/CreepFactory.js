/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('CreepFactory');
 * mod.thing == 'a thing'; // true
 */
 
CreepFactory = {
    parts: {
        MOVE: MOVE,
        WORK: WORK,
        CARRY: CARRY,
        ATTACK: ATTACK,
        RANGED_ATTACK: RANGED_ATTACK,
        HEAL: HEAL,
        TOUGH: TOUGH
    },

    costs: {
        move: 50,
        work: 100,
        carry: 50,
        attack: 80,
        ranged_attack: 150,
        heal: 200,
        tough: 10
    },

    getBodyCost: function(body)
    {
        var cost = 0;
        for(var index in body){
            cost += this.costs[body[index]]
        }
        return cost;
    },
    
    
    generateBody: function(energyLimit, ratios, fillerParts){
        const PARTMAX=50
        var min;
        // determine normalizer := smallest value in ratios
        for(var name in this.costs){
            if(ratios.hasOwnProperty(name)){
                min = name;
                break;
            }
        }
        for(var name in ratios){
            if(ratios[name] < ratios[min])
            {
                min = name;
            }
        }
        
        //Create new associative array of adjusted ratios
        var adjustedRatios = {};
        for(var name in ratios){
            adjustedRatios[name] = ratios[name] / ratios[min];
        }
        
        // determine maximum value for key normalizer
        // and energy cost for minimum configuration
        var baseParts = 0;
        var baseEnergy = 0;
        for(var name in adjustedRatios){
            baseParts += adjustedRatios[name];
            baseEnergy += this.costs[name]*adjustedRatios[name];
        }
        var maxMultiplier = PARTMAX/baseParts;
        var multiplier = Math.floor(Math.min(maxMultiplier, energyLimit/baseEnergy));
        
        //build body based off of normalizer
        var body = []
        for(var name in adjustedRatios){
            for(var i = 0 ; i < adjustedRatios[name]*multiplier ; i++){
                body.push(name);
            }
        }
    
        //if parts remain, use filler part(s) to reach PARTMAX
        if(fillerParts){
            var energyCost = 0;
        for(var index in body){
            energyCost += this.costs[body[index]];
        }
        console.log(body + ', '+ this.getBodyCost(body) +'/' + energyLimit)
        
        var fill = true;
            while(fill === true){
                fill = false;
                for(var i = 0 ; i < fillerParts.length ; i++){
                    var newEnergyCost = energyCost + this.costs[fillerParts[i]];
                    if( newEnergyCost <= energyLimit ){
                        body.push(fillerParts[i]);
                        energyCost = newEnergyCost;
                        fill = true;
                    }
                }
            }
        }
        return body;
    },
}

module.exports = CreepFactory;