var utils = {}
//returns id of closest hostile if one exists
utils.closestHostile = (tower) => {tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS)};

//returns id of closest damaged structure if one exists
utils.closestDamagedStructure = (tower) => {tower.pos.findClosestByRange(FIND_STRUCTURES, {
            filter: (structure) => structure.hits < structure.hitsMax})};

utils.checkEnergy = function(){
    for(var name in Game.rooms) {
        return Game.rooms[name].energyAvailable;
    }
};




module.exports = utils; 

