var manager = require('manager');


var runRoom = function(room){
    manager.creeps.maintainLevels(room);
    manager.creeps.run(room);
    manager.towers.run(room);
}

module.exports.loop = function(){
    for(var room in Game.rooms){
        runRoom(Game.rooms[room]);
    }
}